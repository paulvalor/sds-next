import type { BlokProps } from "lib/storyblok/types"
import type { SbEditableContent } from "storyblok-react"
import type { NavigationItem } from "lib/storyblok/schema"
import type { HTMLAttributes } from "react"

export interface LayoutContext {
  navbarHeight: number
}

export interface LayoutProps {
  navigation: Array<SbEditableContent & {
    menu: NavigationItem[]
  }>
  footer: Array<SbEditableContent & {
    socials: NavigationItem[]
    menu: NavigationItem[]
    copyright: string
  }>
}

export interface NavigationProps extends 
  BlokProps<LayoutProps['navigation'][0]>, 
  HTMLAttributes<HTMLDivElement> {}

export interface FooterProps extends
  BlokProps<LayoutProps['footer'][0]>,
  HTMLAttributes<HTMLDivElement> {}

export interface ItemProps extends
  BlokProps<NavigationItem>,
  HTMLAttributes<HTMLLIElement> {}
