import { useState, useEffect, useRef, createContext } from "react"
import styles from "./styles.module.scss"
import { Footer } from "./components/Footer"
import { Navbar } from "./components/Navbar"

import type { LayoutContext, LayoutProps } from "./types"
import type { FC } from "react"


/** Layout shareable context */
export const Context = createContext<LayoutContext>({ navbarHeight: 0 })

/** Layout entry point  */
export const Layout: FC<LayoutProps> = ({ children, footer, navigation, ...rest }) => {
  const [navbarHeight, setNavbarHeight] = useState(0)
  const navbar = useRef<HTMLDivElement>(null)

  /** Collecting navigation bar height */
  useEffect(() => void setNavbarHeight(navbar.current.clientHeight))

  return (
    <Context.Provider value={{ navbarHeight }}>
      <div className={styles.layout}>
        <Navbar ref={navbar} blok={navigation[0]} {...rest} />
        <main>{children}</main>
        <Footer blok={footer[0]} {...rest} />
      </div>
    </Context.Provider>
  )
}
