import type { NextApiRequest, NextApiResponse } from "next"

/** Exit the current user from "Preview Mode" */
export default async function exitPreviewMode(
  _request: NextApiRequest,
  response: NextApiResponse,
): Promise<void> {
  /** Get current cookies */
  const cookies = response.getHeader('Set-Cookie')
  const newCookies = (Array.isArray(cookies) ? cookies : [cookies ])
    .map(cookie => cookie.toString().replace('SameSite=Lax', 'SameSite=None'))
    
  response.clearPreviewData()
  response.setHeader('Set-Cookie', newCookies)
  response.writeHead(307, { Location: "/" })
  response.end()
}
