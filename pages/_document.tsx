import Document, { Html, Head, Main, NextScript } from "next/document"
import { config } from "lib/storyblok"


export default class MyDocument extends Document {
  public render = () => (
    <Html lang={this.props.locale}>
      <Head>
        <meta charSet="utf-8" />
        <link type="text/plain" rel="author" href="/humans.txt" />
        <script id={config.scriptTagId} src={config.bridgeUri} />
      </Head>
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  )
}
