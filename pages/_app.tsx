import "normalize.css"
import "styles/global.scss"

import type { FC } from 'react'
// import {  } from 'next'

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

// @see https://nextjs.org/docs/advanced-features/measuring-performance
// export function reportWebVitals(metric) {
//   console.log(metric)
// }

export default MyApp
