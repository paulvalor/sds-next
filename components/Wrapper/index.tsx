import { forwardRef } from "react"
import styles from "./styles.module.scss"
import clsx from "clsx"

import type { HTMLAttributes } from "react"

/** Aligns layout horizontaly  */
export const Wrapper = forwardRef<HTMLDivElement, HTMLAttributes<HTMLDivElement>>((
  { children, className, ...rest }, ref,
) => (
  <div className={clsx(styles.wrapper, className)} ref={ref} {...rest}>
    {children}
  </div>
))
