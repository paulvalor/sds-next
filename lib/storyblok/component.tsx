import * as stories from "stories"
import { useStoryblok } from "./hook"

import { FC } from "react"

/** 
 * Incapsulates story into it's parent recursively.
 * On failed stories returns `FallbackComponent`.
 */
export const Recursive: FC<any> = ({ story: { name, content: { parent, ...current }}, ...rest }) => {
  const Story = stories[name] ?? FallbackComponent
  const horizon = <Story signature={name} {...current} {...rest}/>
  return !parent ? horizon : (
    <Recursive story={parent} {...rest}>
      {horizon}
    </Recursive>
  )
}

/** 
 * Renders react tree root.
 * Setups sync data bridge and triggeres recursive rendering. 
 */
export const StoryEntry: FC<any> = ({ story, ...rest }) => {
  const data = useStoryblok(story)
  console.info(data)
  return <Recursive story={data} {...rest} />
}


/** 
 * Gets rendered if relevant story implementation wasn't found.
 * Notifies in console with passed props.
 */
export const FallbackComponent: FC<any> = ({ signature, ...rest }) => {
  console.warn(`Missing "${signature}" story with props: `, rest)
  return (
    <div style={{
      border: '1px dashed orange',
      background: 'rgba(0,0,0,.3)',
      boxSizing: 'border-box',
      color: 'orange',
      position: 'fixed',
      top: '50%',
      left: '50%',
      textAlign: 'center',
      transform: 'translate(-50%, -50%)',
      padding: 16,
    }}>Component for Story <b>"{signature}"</b> wasn't found</div>
  )
}
