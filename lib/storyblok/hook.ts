import { useEffect, useState } from "react"

export function useStoryblok(originalStoryData, isPreview?: boolean) {
  let [storyState, setStoryState] = useState(originalStoryData)

  /**
    * Adds the events for updating the visual editor
    * @see https://bit.ly/3kCsqzc #initializing-the-storyblok-js-bridge
    */
  useEffect(() => {
    /** Only for NextJS preview mode */
    // if (!isPreview) return
    
    /** Setup storyblok context */
    globalThis?.storyblok?.init()

    /** Reload on Next.js page on save or publish event in the Visual Editor */
    globalThis?.storyblok?.on('published', () => location?.reload(true));

    /** Live update the story on input events */
    globalThis?.storyblok?.on('input', ({ story }): void => {
      if (story.content._uid === storyState.content._uid) {
        story.content = globalThis?.storyblok?.addComments(story.content, story.id)
        setStoryState(story)
      }
    })
  })

  return storyState
}
