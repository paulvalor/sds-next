export const scriptTagId = 'storyblokBridge'
export const accessToken = process.env.NEXT_PUBLIC_ACCESS_TOKEN
export const bridgeUri = `https://app.storyblok.com/f/storyblok-latest.js?t=${accessToken}`
